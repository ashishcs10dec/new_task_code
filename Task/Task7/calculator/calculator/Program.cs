﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace calculator
{
    class Program
    {
        static void Main(string[] args)
        {
            try
            {
                int Output = 0;
                String TempList = "";
                String TempGreaterNumList = "";
                bool IsGreaterNum = false;
                Factory Fact = new Factory();
                Console.WriteLine("Please enter the format:");
                String WriteLineText = Console.ReadLine().Trim();
                Factory F = new Factory();
                if (WriteLineText.ToLower().Contains("sum"))
                {
                    TempList = Fact.NormalizeString(WriteLineText);
                    if (TempList.Contains(","))
                    {
                        String[] ExistElement = TempList.Split(',');
                        if (ExistElement.Count() > 1)
                        {
                            for (int i = 0; i < ExistElement.Count(); i++)
                            {
                                if (ExistElement[i] != "")
                                {
                                    if (Convert.ToInt32(ExistElement[i]) > 1000)
                                    {
                                        IsGreaterNum = true;
                                        if (TempGreaterNumList == "")
                                            TempGreaterNumList = ExistElement[i];
                                        else
                                            TempGreaterNumList = TempGreaterNumList + "," + ExistElement[i];
                                    }
                                    else if (Convert.ToInt32(ExistElement[i]) < 1000)
                                    {
                                        if (Output == 0)
                                            Output = Convert.ToInt32(ExistElement[i]);
                                        else
                                            Output = Output + Convert.ToInt32(ExistElement[i]);
                                    }
                                }
                            }
                        }
                    }
                    else
                    {
                        if (TempList == "")
                            Output = 0;
                        else
                            Output = Convert.ToInt32(TempList);
                    }
                }
                else if (WriteLineText.ToLower().Contains("multiply"))
                {
                    TempList = Fact.NormalizeString(WriteLineText);
                    if (TempList.Contains(","))
                    {
                        String[] ExistElement = TempList.Split(',');
                        if (ExistElement.Count() > 1)
                        {
                            for (int i = 0; i < ExistElement.Count(); i++)
                            {
                                if (ExistElement[i] != "")
                                {
                                    if (Convert.ToInt32(ExistElement[i]) > 1000)
                                    {
                                        IsGreaterNum = true;
                                        if (TempGreaterNumList == "")
                                            TempGreaterNumList = ExistElement[i];
                                        else
                                            TempGreaterNumList = TempGreaterNumList + "," + ExistElement[i];
                                    }
                                    else if (Convert.ToInt32(ExistElement[i]) < 1000)
                                    {
                                        if (Output == 0)
                                            Output = Convert.ToInt32(ExistElement[i]);
                                        else
                                            Output = Output * Convert.ToInt32(ExistElement[i]);
                                    }
                                }
                            }
                        }
                    }
                    else
                    {
                        if (TempList == "")
                            Output = 0;
                        else
                            Output = Convert.ToInt32(TempList);
                    }
                }
                else
                {
                    Console.WriteLine("Please use 'sum' or 'multiply' before enter the value.");
                    Console.ReadLine();
                    return;
                }
                if (!IsGreaterNum)
                    Console.WriteLine(Output);
                else
                    Console.WriteLine(Output + Environment.NewLine + "The numbers(" + TempGreaterNumList + ") which are greater than 1000 not allowed.");
                Console.ReadLine();
            }
            catch
            {
                Console.WriteLine("Something went wrong!");
                Console.ReadLine();
            }
        }
    }
}
