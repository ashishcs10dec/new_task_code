﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace calculator
{
    public class Factory
    {
        public String NormalizeString(String WriteLineText)
        {
            string resultString = string.Join(",", Regex.Matches(WriteLineText, @"\d+").OfType<Match>().Select(m => m.Value));
            return resultString;
        }
    }
}
