﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace calculator
{
    class Program
    {
        static void Main(string[] args)
        {
            try
            {
                int Sum = 0;
                String TempList = "";
                Factory Fact = new Factory();
                Console.WriteLine("Please enter the format:");
                String WriteLineText = Console.ReadLine().Trim();
                Factory F = new Factory();
                if (WriteLineText.ToLower().Contains("sum"))
                {
                    TempList = Fact.NormalizeString(WriteLineText);
                    if (TempList.Contains(","))
                    {
                        String[] ExistElement = TempList.Split(',');
                        if (ExistElement.Count() > 1)
                        {
                            for (int i = 0; i < ExistElement.Count(); i++)
                            {
                                if (ExistElement[i] != "")
                                {
                                    if (Sum == 0)
                                        Sum = Convert.ToInt32(ExistElement[i]);
                                    else
                                        Sum = Sum + Convert.ToInt32(ExistElement[i]);
                                }
                            }
                        }
                    }
                    else
                    {
                        if (TempList == "")
                            Sum = 0;
                        else
                            Sum = Convert.ToInt32(TempList);
                    }
                }
                else if (WriteLineText.ToLower().Contains("multiply"))
                {
                    TempList = Fact.NormalizeString(WriteLineText);
                    if (TempList.Contains(","))
                    {
                        String[] ExistElement = TempList.Split(',');
                        if (ExistElement.Count() > 1)
                        {
                            for (int i = 0; i < ExistElement.Count(); i++)
                            {
                                if (ExistElement[i] != "")
                                {
                                    if (Sum == 0)
                                        Sum = Convert.ToInt32(ExistElement[i]);
                                    else
                                        Sum = Sum * Convert.ToInt32(ExistElement[i]);
                                }
                            }
                        }
                    }
                    else
                    {
                        if (TempList == "")
                            Sum = 0;
                        else
                            Sum = Convert.ToInt32(TempList);
                    }
                }
                else
                {
                    Console.WriteLine("Please use 'sum' or 'multiply' before enter the value.");
                    Console.ReadLine();
                    return;
                }
                Console.WriteLine(Sum);
                Console.ReadLine();
            }
            catch
            {
                Console.WriteLine("Something went wrong!");
                Console.ReadLine();
            }
        }
    }
}
